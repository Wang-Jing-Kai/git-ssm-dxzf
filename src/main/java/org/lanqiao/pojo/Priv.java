package org.lanqiao.pojo;
/**
 * POJO
 * @author Administrator
 *
 */
public class Priv {
	private int pid;
	private String pclass;
	private String purl;
	private String name;
	private int state;

	public Priv(int pid, String pclass, String purl, String name, int state) {
		this.pid = pid;
		this.pclass = pclass;
		this.purl = purl;
		this.name = name;
		this.state = state;
	}

	public Priv() {
	}

	public Priv(int pid) {
		this.pid = pid;
	}

	public int getPid() {
		return pid;
	}
	public void setPid(int pid) {
		this.pid = pid;
	}
	public String getPclass() {
		return pclass;
	}
	public void setPclass(String pclass) {
		this.pclass = pclass;
	}
	public String getPurl() {
		return purl;
	}
	public void setPurl(String purl) {
		this.purl = purl;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getState() {
		return state;
	}
	public void setState(int state) {
		this.state = state;
	}
}
