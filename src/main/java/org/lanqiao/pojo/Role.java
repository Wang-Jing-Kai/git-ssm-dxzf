package org.lanqiao.pojo;

import java.util.Date;
import java.util.List;

public class Role {
	private Integer id;
	private String rname;
	private String by001;//存放图片的url地址
	private List<Priv> lp; //一个角色有多个权限
	private String createUser;
	private Date createTime;



	public String getCreateUser() {
		return createUser;
	}

	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public String getPrivs() {
		String s = "";
		if(lp != null) {
			for(Priv p : lp) {
				s+=p.getName()+"、";
			}
		}
		return s;
	}


	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getRname() {
		return rname;
	}
	public void setRname(String rname) {
		this.rname = rname;
	}
	public List<Priv> getLp() {
		return lp;
	}
	public void setLp(List<Priv> lp) {
		this.lp = lp;
	}


	public String getBy001() {
		return by001;
	}


	public void setBy001(String by001) {
		this.by001 = by001;
	}
	
}
