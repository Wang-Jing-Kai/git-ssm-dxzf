package org.lanqiao.controller;

import org.lanqiao.pojo.Priv;
import org.lanqiao.pojo.Role;
import org.lanqiao.service.PrivService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@Controller
@RequestMapping("/priv")
public class PrivController {
    @Autowired
    @Qualifier("PrivServiceImpl")
    private PrivService privService;

    public void setPrivService(PrivService privService) {
        this.privService = privService;
    }

    @RequestMapping("/add")
    public String roleList(Model model){
        List<Priv> lp = privService.getAllPrivs();
        model.addAttribute("lp",lp);
        return "forward:/WEB-INF/view/role/role_add.jsp";
    }
}
