package org.lanqiao.controller;

import com.github.pagehelper.PageInfo;
import org.lanqiao.pojo.Priv;
import org.lanqiao.pojo.Role;
import org.lanqiao.service.PrivService;
import org.lanqiao.service.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("/role")
public class RoleController {
    @Autowired
    @Qualifier("RoleServiceImpl")
    private RoleService roleService;

    @Autowired
    @Qualifier("PrivServiceImpl")
    private PrivService privService;

    public void setRoleService(RoleService roleService) {
        this.roleService = roleService;
    }

    public void setPrivService(PrivService privService) {
        this.privService = privService;
    }

    //显示角色列表
    @RequestMapping("/roleList")
    //@RequestParam(name="pageNum",required = false defaultValue = "1")代表访问时不需要传入这个参数,默认为1
    public String roleList(Model model,@RequestParam(name="pageNum",required = false,defaultValue = "1") int pageNum){
        PageInfo<Role> list = roleService.getAllRoles2(pageNum);
        model.addAttribute("pageInfo",list);
        //等同 return "view/role/role_list"
        return "forward:/WEB-INF/view/role/role_list.jsp";
    }
    //显示修改角色列表
    @RequestMapping("/updateList")
    public String updeteList(Model model,String rid){
        Role role = roleService.getRolePrivByRid(Integer.parseInt(rid.trim()));
        List<Priv> lp = privService.getAllPrivs();
        model.addAttribute("role",role);
        model.addAttribute("lp",lp);
        return "forward:/WEB-INF/view/role/role_modi.jsp";
    }

    //添加角色
        @RequestMapping("/add")
        public String addPriv(String rname,String[] privs,Model model){
            Role role = new Role();
            role.setRname(rname);
            ArrayList<String> list = new ArrayList<>();
            for (String priv : privs) {
                list.add(priv);
            }
            boolean b1 = roleService.addRole(role);
            boolean b = roleService.addRolePriv(rname, list);
            if(b) {
                //添加成功
                model.addAttribute("state", "1");
            }else {
                //添加失败
                model.addAttribute("state", "0");
            }
            return "forward:/priv/add";
    }

    //删除角色
        @RequestMapping("/delete")
        public String deltePriv(String rid,Model model){
            boolean b = roleService.delRoleByRid(Integer.parseInt(rid));
            if(b) {
                //删除成功
                model.addAttribute("state", "1");
            }else {
                //删除失败
                model.addAttribute("state", "0");
            }
            return "forward:/role/roleList";
        }

        //修改角色
        @RequestMapping("/update")
        public String update(String rname, String rid,String[] privs,Model model){
            Role role = new Role();
            role.setRname(rname);
            role.setId(Integer.parseInt(rid.trim()));
            ArrayList<String> list = new ArrayList<>();
            for (String priv : privs) {
                list.add(priv);
            }
            boolean b = roleService.updateRole(role, list);
            if(b) {
                //修改成功
                model.addAttribute("state", "1");
            }else {
                //修改失败
                model.addAttribute("state", "0");
            }
            return "forward:/role/updateList";
        }
}
