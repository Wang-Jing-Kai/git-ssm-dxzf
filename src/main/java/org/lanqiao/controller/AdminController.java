package org.lanqiao.controller;

import org.lanqiao.pojo.Admin;
import org.lanqiao.service.AdminService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.Random;

@Controller
@RequestMapping("/admin")
public class AdminController {
    @Autowired
    @Qualifier("AdminServiceImpl")
    private AdminService adminService;

    public void setAdminService(AdminService adminService) {
        this.adminService = adminService;
    }

    @RequestMapping("/index")
    public String index(){
        return "view/index";
    }

    @RequestMapping("/login")
    public String login(Model model, HttpSession session,String acname,String apwd,String vcode){
        String checkCode = (String) session.getAttribute("vcode");
        if (!checkCode.equalsIgnoreCase(vcode)){
            model.addAttribute("errorMsg","验证码错误");
            return  "forward:/login.jsp";
        }
        Admin admin = adminService.checkUser(acname, apwd);
        if (admin==null){
            model.addAttribute("errorMsg","用户名或密码错误");
            return  "forward:/login.jsp";
        }else {
            session.setAttribute("admin",admin);
            return "view/index";
        }
    }
    //跳转到修改密码页面
    @RequestMapping("/toUpdateAdmin")
    public String toUpdatePage(){
        return "view/user/user_modi_pwd";
    }
    //修改用户
    @RequestMapping("/updatePwd")
    public String updatePage(String old_pwd,String apwd,HttpSession session,Model model) {
        Admin admin = (Admin)session.getAttribute("admin");
        System.out.println(admin.getId());
        if (!admin.getApwd().equals(old_pwd)){
            model.addAttribute("errorMsg", "对不起，旧密码不正确");
            return "view/user/user_modi_pwd";
        }
        boolean b = adminService.updateUserPassword(admin.getId(), apwd);
        if (b){
            session.invalidate();
            return  "redirect:/login.jsp";
        }{
            model.addAttribute("errorMsg", "对不起，修改密码失败");
            return "view/user/user_modi_pwd";
        }
    }

    //修改个人信息  /ResponseBody 跳过视图解析器
    @RequestMapping("/UpdateUserInfo")
    @ResponseBody
    public void updateUserInfo(HttpServletResponse response,String aname, String atel, String aemail, String id, HttpSession session,Model model)  {
        Admin admin = new Admin(Integer.parseInt(id),aname,atel,aemail);
        boolean b = adminService.updateAdminInfo(admin);
        Admin admin1 = (Admin) session.getAttribute("admin");
        admin1.setAname(aname);
        admin1.setAtel(atel);
        admin1.setAemail(aemail);
        if (b){
            session.setAttribute("admin",admin1);
            try {
                response.setCharacterEncoding("utf-8");
                response.getWriter().println("更新成功");
            } catch (IOException e) {
                e.printStackTrace();
            }
        }else {
            try {
                response.setCharacterEncoding("utf-8");
                response.getWriter().println("更新失败");
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    @RequestMapping("/exit")
    public String exit(HttpSession session){
        session.invalidate();
        return  "redirect:/ .jsp";
    }
    @RequestMapping("/toUserInfo")
    public String toUserInfo(){
        return "/view/user/user_info";
    }

    //验证码
    @RequestMapping("/checkCode")
    @ResponseBody
    public void checkCode(HttpSession session,HttpServletResponse response) throws IOException {
        //服务器通知浏览器不要缓存
        response.setHeader("pragma","no-cache");
        response.setHeader("cache-control","no-cache");
        response.setHeader("expires","0");

        //在内存中创建一个长80，宽30的图片，默认黑色背景
        //参数一：长
        //参数二：宽
        //参数三：颜色
        int width = 80;
        int height = 30;
        BufferedImage image = new BufferedImage(width,height,BufferedImage.TYPE_INT_RGB);

        //获取画笔
        Graphics g = image.getGraphics();
        //设置画笔颜色为灰色
        g.setColor(Color.GRAY);
        //填充图片
        g.fillRect(0,0, width,height);

        //产生4个随机验证码，12Ey
        String checkCode = getCheckCode();
        //将验证码放入HttpSession中
        session.setAttribute("vcode",checkCode);

        //设置画笔颜色为黄色
        g.setColor(Color.YELLOW);
        //设置字体的小大
        g.setFont(new Font("黑体",Font.BOLD,24));
        //向图片上写入验证码
        g.drawString(checkCode,15,25);

        //将内存中的图片输出到浏览器
        //参数一：图片对象
        //参数二：图片的格式，如PNG,JPG,GIF
        //参数三：图片输出到哪里去
        ImageIO.write(image,"PNG",response.getOutputStream());
    }   
    /**
     * 产生4位随机字符串
     */
    private String getCheckCode() {
        String base = "0123456789ABCDEFGabcdefg";
        int size = base.length();
        Random r = new Random();
        StringBuffer sb = new StringBuffer();
        for(int i=1;i<=4;i++){
            //产生0到size-1的随机值
            int index = r.nextInt(size);
            //在base字符串中获取下标为index的字符
            char c = base.charAt(index);
            //将c放入到StringBuffer中去
            sb.append(c);
        }
        return sb.toString();
    }
}
