package org.lanqiao.service;

import org.lanqiao.dao.PrivMapper;
import org.lanqiao.pojo.Priv;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class PrivServiceImpl implements  PrivService{
    @Autowired
    private PrivMapper privMapper;

    public void setPrivMapper(PrivMapper privMapper) {
        this.privMapper = privMapper;
    }

    @Override
    public List<Priv> getAllPrivs() {
        return privMapper.selectAllPrivs();
    }
}
