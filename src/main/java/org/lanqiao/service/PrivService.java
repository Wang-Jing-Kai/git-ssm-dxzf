package org.lanqiao.service;

import org.apache.ibatis.annotations.Param;
import org.lanqiao.pojo.Priv;

import java.util.List;

public interface PrivService {

    /**
     * 获取所有权限
     * @return
     */
    List<Priv> getAllPrivs();
}
