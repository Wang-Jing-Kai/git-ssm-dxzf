package org.lanqiao.service;


import org.lanqiao.dao.AdminMapper;
import org.lanqiao.dao.PrivMapper;
import org.lanqiao.dao.RoleMapper;
import org.lanqiao.pojo.Admin;
import org.lanqiao.pojo.Priv;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AdminServiceImpl implements AdminService {
	@Autowired
	private AdminMapper adminMapper;
	@Autowired
	private PrivMapper privMapper;
	@Autowired
	private RoleMapper roleMapper;


	public void setAdminMapper(AdminMapper adminMapper) {
		this.adminMapper = adminMapper;
	}
	public void setPrivMapper(PrivMapper privMapper) {
		this.privMapper = privMapper;
	}
	public void setRoleMapper(RoleMapper roleMapper) {
		this.roleMapper = roleMapper;
	}

	@Override
	public Admin checkUser(String acname, String apwd) {
		Admin admin = adminMapper.selectAdminByAcnameAndApwd(acname, apwd);
		if (admin != null){
			admin.setLp(privMapper.selectPrivsByAid(admin.getId()));
			admin.setLr(roleMapper.selectRolesByAid(admin.getId()));
		}
		return admin;
	}

	@Override
	public boolean updateUserPassword(int id, String apwd) {
		return adminMapper.updatePassword(id,apwd)>0;
	}

	@Override
	public boolean updateAdminInfo(Admin admin) {
		return adminMapper.updateAdminInfo(admin)>0;
	}


}
