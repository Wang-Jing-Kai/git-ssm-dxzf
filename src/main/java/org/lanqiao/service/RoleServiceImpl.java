package org.lanqiao.service;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.lanqiao.dao.PrivMapper;
import org.lanqiao.dao.RoleMapper;
import org.lanqiao.pojo.Admin;
import org.lanqiao.pojo.Priv;
import org.lanqiao.pojo.Role;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public class RoleServiceImpl implements RoleService{
    @Autowired
    private RoleMapper roleMapper;
    @Autowired
    private PrivMapper privMapper;

    public void setRoleMapper(RoleMapper roleMapper) {
        this.roleMapper = roleMapper;
    }
    public void setPrivMapper(PrivMapper privMapper) {
        this.privMapper = privMapper;
    }
    @Override
    public List<Role> getAllRoles() {
        List<Role> list = roleMapper.selectAllRoles();
        for (Role role : list) {
            List<Priv> lp = privMapper.selectPrivsByRid(role.getId());
            role.setLp(lp);
        }
        return list;
    }

    @Override
    public PageInfo<Role> getAllRoles2(int pageNum) {
        //1、把分页信息添加给pagehelper控件，以便拦截器使用
        PageHelper.startPage(pageNum,2);//当前页面，每页记录数
        List<Role> list = roleMapper.selectAllRoles();
        for (Role role : list) {
            List<Priv> lp = privMapper.selectPrivsByRid(role.getId());
            role.setLp(lp);
        }
        //2、把查询的结果封装到pageInfo中
        PageInfo<Role> pageInfo = new PageInfo<>(list);
        return pageInfo;
    }

    @Override
    public boolean addRolePriv(String rname,List<String> list) {
        //Role role = roleMapper.selectRoleByRname(rname);
        Role role = new Role();
        role.setRname(rname);
        Role role1 = roleMapper.selectRoleByRname(rname);
        role.setId(role1.getId());
        int j = roleMapper.insertRolePriv(role.getId(), list);
        return j>0;
    }

    @Override
    public boolean addRole(Role role) {
        int i = roleMapper.insertRole(role);
        return i>0;
    }


    @Override
    public boolean delRoleByRid(Integer rid) {
        List<Admin> admins = roleMapper.selectAdminsByRid(rid);
        System.out.println(admins);
        if (admins.size()==0){
            int i = roleMapper.deleteRolePrivByRid(rid);
            int j = roleMapper.deleteRoleByRid(rid);
            return i>0&&j>0;
        }
        return false;
    }

    @Override
    public Role getRolePrivByRid(Integer rid) {
        Role role = roleMapper.selectRoleByRid(rid);
        List<Priv> lp = privMapper.selectPrivsByRid(rid);
        role.setLp(lp);
        return role;
    }

    @Override
    public boolean updateRole(Role role,List<String> list) {
        int i = roleMapper.updateRole(role);
        int j = roleMapper.deleteRolePrivByRid(role.getId());
        String privs = role.getPrivs();
        int k = roleMapper.insertRolePriv(role.getId(), list);
        return i>0&&j>0&&k>0;
    }
    
}
