package org.lanqiao.service;

import com.github.pagehelper.PageInfo;
import org.apache.ibatis.annotations.Param;
import org.lanqiao.pojo.Priv;
import org.lanqiao.pojo.Role;

import java.util.List;
import java.util.Map;

public interface RoleService {
    /**
     * 获取所有的角色
     * @return
     */
    List<Role> getAllRoles();

    /**
     * 获取所有的角色 带分页
     * @param pageNum 当前第几页
     * @return
     */
    PageInfo<Role> getAllRoles2(int pageNum);

    /**
     * 添加角色
     * @param list
     * @return
     */
    boolean addRolePriv(String rname,List<String> list);

    boolean addRole(Role role);

    boolean delRoleByRid(Integer rid);

    /**
     * 通过角色id获取角色所有信息 包括权限
     * @return
     */
    Role getRolePrivByRid(Integer rid);

    /**
     * 修改角色
     * @param role
     * @return
     */
    boolean updateRole(Role role,List<String> list);



}
