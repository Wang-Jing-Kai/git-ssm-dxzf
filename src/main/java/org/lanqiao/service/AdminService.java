package org.lanqiao.service;


import org.lanqiao.pojo.Admin;

public interface AdminService {
	Admin checkUser(String acname , String apwd);

	boolean updateUserPassword(int id,String apwd);

	/**
	 * 修改个人信息
	 * @param admin
	 * @return
	 */
	boolean updateAdminInfo(Admin admin);

}
