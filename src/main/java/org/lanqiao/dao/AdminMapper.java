package org.lanqiao.dao;


import org.apache.ibatis.annotations.Param;
import org.lanqiao.pojo.Admin;

public interface AdminMapper {
	/**
	 * 登录验证-根据账号和密码查询该用户信息并验证登录
	 * @param acname 账号
	 * @param apwd 密码
	 * @return
	 */
	Admin selectAdminByAcnameAndApwd(@Param("acname") String acname ,@Param("apwd") String apwd);

	int updatePassword(@Param("id") int id,@Param("apwd") String apwd);

	/**
	 * 修改个人信息
	 * @param admin
	 * @return
	 */
	int updateAdminInfo(Admin admin);
}
