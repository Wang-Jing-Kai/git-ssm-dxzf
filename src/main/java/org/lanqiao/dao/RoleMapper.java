package org.lanqiao.dao;

import org.apache.ibatis.annotations.Param;
import org.lanqiao.pojo.Admin;
import org.lanqiao.pojo.Role;

import java.util.List;
import java.util.Map;

public interface RoleMapper {
    List<Role> selectRolesByAid(@Param("aid") Integer aid);

    List<Role> selectAllRoles();

    int insertRole(Role role);

    /**
     * 添加角色
     * @param list
     * @return
     */
    int insertRolePriv(@Param("rid")Integer rid,@Param("list") List<String> list);
    int insertRolePrivs(Role role);
    /**
     * 根据角色名称查询rid
     * @param rname
     * @return
     */
    Role selectRoleByRname(@Param("rname") String rname);

    Role selectRoleByRid(@Param("rid") Integer rid);
    /**
     * 根据rid查询时候有对于的关联用户
     * @param rid
     * @return true 存在关联， false 不存在关联
     */
    List<Admin> selectAdminsByRid(@Param("rid") Integer rid);

    int deleteRolePrivByRid(@Param("rid") Integer rid);

    int deleteRoleByRid(@Param("rid") Integer rid);

    /**
     * 修改角色
     * @param role
     * @return
     */
    int updateRole(Role role);



}
