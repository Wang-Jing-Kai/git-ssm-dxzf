package org.lanqiao.dao;

import org.apache.ibatis.annotations.Param;
import org.lanqiao.pojo.Priv;

import java.util.List;

public interface PrivMapper {
    /**
     * 通过用户id获取该用户所有权限
     * @param aid
     * @return
     */
    public List<Priv> selectPrivsByAid(@Param("aid") Integer aid);

    /**
     * 通过角色id获取用户所有权限
     * @param rid
     * @return
     */
    public List<Priv> selectPrivsByRid(@Param("rid") Integer rid);

    public List<Priv> selectAllPrivs();
}
