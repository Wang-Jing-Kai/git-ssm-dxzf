import org.junit.Test;
import org.lanqiao.pojo.Admin;
import org.lanqiao.service.AdminService;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Mytest {
    @Test
    public void test(){
        ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
        AdminService adminServiceImpl = context.getBean("AdminServiceImpl", AdminService.class);
        Admin lisi = adminServiceImpl.checkUser("lisi", "123");
        System.out.println(lisi);

    }
}
